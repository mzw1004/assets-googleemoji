# -*- coding: utf-8 -*-

import json
import os
import math
from PIL import Image

categories = ['/people/', '/nature/', '/food-drink/', '/activity/',
              '/travel-places/', '/objects/', "/symbols/", '/flags/']
EMOJI_SIZE = 128


def load_config():
    emoji_json = {}
    emoji_json['emojiSize'] = EMOJI_SIZE
    cate_list = []
    emoji_json['data'] = cate_list
    out_dir = 'assets/emoji'
    with open('data.json') as json_file:
        data = json.load(json_file)
        for cate in categories:
            path_list = []
            for names in data[cate]:
                for name in names.split(", "):
                    path = match_emoji_image(name)
                    if path in path_list:
                        continue
                    if os.path.exists(path):
                        path_list.append(path)
                    # else:
                    #     print(path, "not exist")
            cate = cate.replace('/', '')
            file_name = "v1_cate_" + cate + '_128.png'
            count, col = gen_texture_pack(out_dir, file_name, path_list)
            cate_json = {'label': cate, 'count': count, 'column': col, 'path': file_name}
            cate_list.append(cate_json)

    with open(out_dir + "/config.json", 'w') as outfile:
        json.dump(emoji_json, outfile)


def match_emoji_image(name):
    name_str = str(name)
    name_str = name_str.replace('+', '')
    name_str.split()
    return 'noto-emoji/png/128/emoji_' + name_str.lower() + '.png'


def gen_texture_pack(dir, out_file, path_list):
    all_size = len(path_list)
    line_num = math.floor(math.sqrt(all_size) + 0.5)
    print('total:', all_size, 'line:', line_num)
    width = EMOJI_SIZE * line_num
    height = EMOJI_SIZE * (all_size // line_num)
    if all_size % line_num > 0:
        height += EMOJI_SIZE
    image = Image.new('RGBA', (width, height))
    for i in range(all_size):
        try:
            emoji = Image.open(path_list[i])
            left = i % line_num * EMOJI_SIZE
            top = i // line_num * EMOJI_SIZE
            right = left + EMOJI_SIZE
            bottom = top + EMOJI_SIZE
            image.paste(emoji, (left, top, right, bottom))
        except:
            print(path_list[i], "not exist")

    if not os.path.exists(dir):
        os.makedirs(dir)
    image.save(dir + "/" + out_file, quality=100)
    return all_size, line_num


if __name__ == '__main__':
    load_config()
