# -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup
import json

categories = ['/people/', '/nature/', '/food-drink/', '/activity/',
              '/travel-places/', '/objects/', "/symbols/", '/flags/']


def main():
    result = {}

    with open('categories.json') as json_file:
        data = json.load(json_file)
        for cate in categories:
            result[cate] = read_cate(data[cate])

    with open('data.json', 'w') as outfile:
        json.dump(result, outfile)


def read_cate(urls):
    list = []

    print(urls)
    i = 0
    while i < len(urls):
        try:
            list.append(get_emoji_link(urls[i]))
            i += 1
        except:
            print(urls[i], 'failed!!')

    return list


def list_categories(name):
    result = []
    r = requests.get("https://emojipedia.org" + name)
    print(r.status_code)

    soup = BeautifulSoup(r.content, features="html.parser")
    for ultag in soup.find_all('ul', {'class': 'emoji-list'}):
        for litag in ultag.find_all('li'):
            result.append(litag.a['href'])

    r.close()
    return result


def get_emoji_link(link):
    r = requests.get("https://emojipedia.org" + link)
    print(link, r.status_code)
    soup = BeautifulSoup(r.content, features="html.parser")
    r.close()
    for article in soup.find_all('article'):
        for a in article.find_all('a'):
            if a['href'].startswith('/emoji/'):
                return get_codepoints(a['href'])


def get_codepoints(link):
    r = requests.get("https://emojipedia.org" + link)
    print(link, r.status_code)
    soup = BeautifulSoup(r.content, features="html.parser")
    r.close()
    for td in soup.find_all('td'):
        if td.text.startswith('U'):
            print(td.text)
            return td.text


if __name__ == '__main__':
    main()
